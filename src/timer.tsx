import React, { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button';
import { Card } from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

export const Timer: React.FC = () => {

    const customStyle = makeStyles(theme => ({
        Typography: {
            backgroundColor: '#cfe8fc',
            height: '100vh'
        },
        ChildTypography: {
            backgroundColor: '#cfe8fc',
            height: '20%',
            padding: '10px'
        },
        Button: {
            marginRight: '10px'
        },
        Card: {
            marginTop: '10px',
            padding: '10px',


        },


    }));

    const StyleClass = customStyle();

    const [timerOneSeconds, setTimerOneSeconds] = useState(0);
    const [timerOneTime, setTimerOneTime] = useState('00:00.000');

    const [timerTwoSeconds, setTimerTwoSeconds] = useState(0);
    const [timerTwoTime, setTimerTwoTime] = useState('00:00.000');

    const [timerThreeSeconds, setTimerThreeSeconds] = useState(0);
    const [timerThreeTime, setTimerThreeTime] = useState('00:00.000');

    const [timerThreeMilliseconds, setTimerThreeMilliseconds] = useState(0);
    const [timerThreesecondsForTotal, setTimerThreesecondsForTotal] = useState(0);

    const [isActiveTimerOne, setIsActiveTimerOne] = useState(false);
    const [isActiveTimerTwo, setIsActiveTimerTwo] = useState(false);
    const [isActiveTimerThree, setIsActiveTimerThree] = useState(false);

    function timerOneToggle() {
        setIsActiveTimerOne(!isActiveTimerOne);
    }

    function resetTimerOne() {
        setTimerOneTime('00:00.000');
        setTimerOneSeconds(0);
        setIsActiveTimerOne(false);
    }

    function timerTwoToggle() {

        setIsActiveTimerTwo(!isActiveTimerTwo);
    }
    function resetTimerTwo() {
        setTimerTwoTime('00:00.000');
        setTimerTwoSeconds(0);
        setIsActiveTimerTwo(false);
    }

    function timerThreeToggle() {
        setIsActiveTimerThree(!isActiveTimerThree);
    }

    function resetTimerThree() {
        setTimerThreeTime('00:00.000');
        setTimerThreesecondsForTotal(0);
        setTimerThreeMilliseconds(0);
        setTimerThreeSeconds(0);
        setIsActiveTimerThree(false);
    }


    useEffect(() => {
        let interval: any = null;
        if (isActiveTimerOne) {
            interval = setInterval(() => {
                setTimerOneSeconds(seconds => seconds + 10);

            }, 10000);
            let time = timerOneSeconds;
            let minutes = Math.floor(timerOneSeconds / 60);
            let remindSeconds = time - minutes * 60;

            setTimerOneTime(minutes + ':' + remindSeconds + '.000');


        } else if (!isActiveTimerOne && timerOneSeconds !== 0) {
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [isActiveTimerOne, timerOneSeconds]);


    useEffect(() => {
        let interval: any = null;
        if (isActiveTimerTwo) {
            interval = setInterval(() => {
                setTimerTwoSeconds(seconds => seconds + 1);

            }, 1000);


            let time = timerTwoSeconds;
            let minutes = Math.floor(timerTwoSeconds / 60);
            let remindSeconds = time - minutes * 60;
            setTimerTwoTime(minutes + ':' + remindSeconds + '.000');

        } else if (!isActiveTimerTwo && timerTwoSeconds !== 0) {

            clearInterval(interval);
        }

        return () => clearInterval(interval);
    }, [isActiveTimerTwo, timerTwoSeconds]);

    useEffect(() => {

        let interval: any = null;
        if (isActiveTimerThree) {
            interval = setInterval(() => {

                setTimerThreeSeconds(seconds => seconds + 0.1);

            }, 100);



            let time = parseFloat(timerThreeSeconds.toFixed(3));
            let milliseconds = timerThreeSeconds.toFixed(3).split(".")[1];
            setTimerThreeMilliseconds(parseInt(milliseconds));
            let minutes = Math.floor((parseFloat(timerThreeSeconds.toFixed(3))) / 60);
            let remindSeconds = time - minutes * 60;
            setTimerThreesecondsForTotal(Math.floor(time));
            setTimerThreeTime(minutes + ':' + Math.floor(remindSeconds) + '.' + milliseconds);

        } else if (!isActiveTimerThree && timerThreeSeconds !== 0) {

            clearInterval(interval);
        }

        return () => clearInterval(interval);
    }, [isActiveTimerThree, timerThreeSeconds]);


    let totalSeconds: any = timerOneSeconds + timerTwoSeconds + timerThreesecondsForTotal;

    let time = totalSeconds;
    let minutes = Math.floor(totalSeconds / 60);
    let remindSeconds = time - minutes * 60;

    let totalTime = minutes + ':' + remindSeconds + '.' + timerThreeMilliseconds;

    return (<div>
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="sm"  >
                <Typography component="div" className={StyleClass.Typography} >

                    <Container maxWidth="sm">

                        <Grid container>
                            <Grid item xs={6}>
                                <Typography component="div" className={StyleClass.ChildTypography} >
                                    <Card className={StyleClass.Card} color="primary">
                                        <div style={{ textAlign: 'center' }}>
                                            <h3>Total Timer</h3>
                                            {totalTime}

                                        </div>
                                    </Card>


                                </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography component="div" className={StyleClass.ChildTypography} >


                                    <Card color="primary" className={StyleClass.Card}>
                                        <div style={{ textAlign: 'center' }}>
                                            <h3>Timer 1</h3>
                                            {timerOneTime}
                                        </div>
                                        <div style={{ textAlign: 'center' }}>
                                            <Button variant="contained" className={StyleClass.Button} color="primary" onClick={timerOneToggle}>
                                                {isActiveTimerOne ? 'Pause' : 'Start'}
                                            </Button>
                                            <Button variant="contained" color="primary" onClick={resetTimerOne}>
                                                Reset
                                          </Button>
                                        </div>

                                    </Card>


                                    <Card color="primary" className={StyleClass.Card}>
                                        <div style={{ textAlign: 'center' }}>
                                            <h3>Timer 2</h3>
                                            {timerTwoTime}

                                        </div>
                                        <div style={{ textAlign: 'center' }}>
                                            <Button variant="contained" className={StyleClass.Button} color="primary" onClick={timerTwoToggle}>
                                                {isActiveTimerTwo ? 'Pause' : 'Start'}
                                            </Button>
                                            <Button variant="contained" color="primary" onClick={resetTimerTwo}>
                                                Reset
                                         </Button>
                                        </div>

                                    </Card>


                                    <Card color="primary" className={StyleClass.Card}>
                                        <div style={{ textAlign: 'center' }}>
                                            <h3>Timer 3</h3>
                                            {timerThreeTime}
                                        </div>
                                        <div style={{ textAlign: 'center' }}>
                                            <Button variant="contained" className={StyleClass.Button} color="primary"
                                                onClick={timerThreeToggle}>
                                                {isActiveTimerThree ? 'Pause' : 'Start'}
                                            </Button>
                                            <Button variant="contained" color="primary" onClick={resetTimerThree}>
                                                Reset
                                             </Button>
                                        </div>

                                    </Card>
                                </Typography>
                            </Grid>
                        </Grid>


                    </Container>
                </Typography>

            </Container>
        </React.Fragment>



    </div>);

};



